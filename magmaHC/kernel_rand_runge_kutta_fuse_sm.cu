#ifndef kernel_rand_runge_kutta_fuse_sm_cu
#define kernel_rand_runge_kutta_fuse_sm_cu
// ============================================================================
// randomized Runge-Kutta method using fused kernel, shared memory
//
// Modifications
//    Chien  21-05-05:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// cuda included
#include <cuda.h>
#include <cuda_runtime.h>

// magma
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- device function --
#include "magmaHC-dev.cuh"
#include "magmaHC-kernels.h"

// === magma ===
// use this so magmasubs will replace with relevant precision, so we can comment out
// the switch case that causes compilation failure
#define PRECISION_c
#ifdef PRECISION_z
#define MAX_N    (53)
#else
#define MAX_N    (60)
#endif

#define SLDA(n)  ( (n == 7 || n == 15 || n == 23 || n == 31) ? (n) : (n+1) )
#define sA(i,j)  sA[(j)*SLDA(N) + (i)]
#define sB(i,j)  sB[(j)*SLDA(N) + (i)]
#define sS(i,j)  sS[(j)*SLDA(N) + (i)]

namespace magmaHCWrapper {

  template<int N>
  __global__ void
  rand_runge_kutta_fuse_sm(
    magmaFloatComplex** dA_array, magma_int_t ldda, magma_int_t** dipiv_array,
    magmaFloatComplex **dB_array, magma_int_t* dinfo_array,
    magmaFloatComplex **dS_array)
  {
    extern __shared__ magmaFloatComplex zdata[];
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x ;

    magmaFloatComplex* dA = dA_array[batchid];
    magmaFloatComplex* dB = dB_array[batchid];
    magmaFloatComplex* dS = dS_array[batchid];
    //extern __shared__ magmaFloatComplex zS[];
    magma_int_t* ipiv = dipiv_array[batchid];

    magmaFloatComplex rA[N]  = {MAGMA_C_ZERO};
    int linfo = 0, rowid = tx;
    int update_rowid;

    magmaFloatComplex  rB = MAGMA_C_ZERO;
    magmaFloatComplex *sB = (magmaFloatComplex*)(zdata);
    magmaFloatComplex *sx = sB + N;
    double* dsx = (double*)(sx + N);
    int* sipiv = (int*)(dsx + N);
    magmaFloatComplex *sS = (magmaFloatComplex*)(sipiv + N);

    // -- read (initialize) --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] = dA[ i * ldda + tx ];
    }
    rB = dB[tx];
    sS[tx] = dS[tx];
    __syncthreads();

    // -- solve for k1 --
    cgesv_batched_small_device<N>( tx, rA, sipiv, rB, sB, sx, dsx, rowid, linfo );
    magmablas_syncwarp();
    if(tx == 0){
        dinfo_array[batchid] = (magma_int_t)( linfo );
    }
    ipiv[ tx ] = (magma_int_t)(sipiv[tx] + 1);    // fortran indexing

    // -- create new A and b --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] += 1;
    }
    rB = sB[rowid] + 1;

    // -- compute k1*1./6. storing in r_S --
    sS[rowid] = dS[rowid] + sB[rowid] * 1.0/6.0;
    __syncthreads();
    update_rowid = rowid;

    // -- solve for k2 --
    cgesv_batched_small_device<N>( tx, rA, sipiv, rB, sB, sx, dsx, rowid, linfo );
    magmablas_syncwarp();
    if(tx == 0){
        dinfo_array[batchid] = (magma_int_t)( linfo );
    }
    ipiv[ tx ] = (magma_int_t)(sipiv[tx] + 1);    // fortran indexing

    // -- create new A and b --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] += 1;
    }
    rB = sB[rowid] + 1;

    /*if (update_rowid == 1) {
      printf("(%f, %f), (%f, %f)\n", MAGMA_Z_REAL(sS[update_rowid]), MAGMA_Z_IMAG(sS[update_rowid]), MAGMA_Z_REAL(sB[update_rowid]), MAGMA_Z_IMAG(sB[update_rowid]));
    }*/

    // -- compute k1*1./6. + k2*1./3. storing in r_S --
    sS[update_rowid] += sB[update_rowid] * 1.0/3.0;
    __syncthreads();

    // -- solve for k3 --
    cgesv_batched_small_device<N>( tx, rA, sipiv, rB, sB, sx, dsx, rowid, linfo );
    magmablas_syncwarp();
    if(tx == 0){
        dinfo_array[batchid] = (magma_int_t)( linfo );
    }
    ipiv[ tx ] = (magma_int_t)(sipiv[tx] + 1);    // fortran indexing

    // -- create new A and b --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] += 1;
    }
    rB = sB[rowid] + 1;

    // -- compute k1*1./6. + k2*1./3. + k3*1./3. storing in sS --
    sS[update_rowid] += sB[update_rowid] * 1.0/3.0;
    __syncthreads();

    // -- solve for k4 --
    cgesv_batched_small_device<N>( tx, rA, sipiv, rB, sB, sx, dsx, rowid, linfo );
    magmablas_syncwarp();
    if(tx == 0){
        dinfo_array[batchid] = (magma_int_t)( linfo );
    }
    ipiv[ tx ] = (magma_int_t)(sipiv[tx] + 1);    // fortran indexing

    // -- compute k1*1./6. + k2*1./3. + k3*1./3. + k4*1./6. storing in sS --
    sS[update_rowid] += sB[update_rowid] * 1.0/6.0;
    __syncthreads();

    // -- move rA, sB, and rS back to dA, dB, and dS --
    #pragma unroll
    for(int i = 0; i < N; i++){
        dA[ i * ldda + rowid ] = rA[i];
    }
    //dB[ rowid ]   = rB;
    dB[ tx ] = sB[tx];

    dS[update_rowid] = sS[update_rowid];
  }

  extern "C" void
  kernel_rand_runge_kutta_fuse_sm(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda, magma_int_t** dipiv_array,
    magmaFloatComplex **dB_array, magma_int_t* dinfo_array,
    magmaFloatComplex **dS_array)
  {
    const magma_int_t thread_x = N;
    dim3 threads(thread_x, 1, 1);
    dim3 grid(batchCount, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    magma_int_t shmem  = 0;
    shmem += N * sizeof(magmaFloatComplex); // B
    shmem += N * sizeof(magmaFloatComplex); // sx
    shmem += N * sizeof(double);             // dsx
    shmem += N * sizeof(int);                // pivot
    shmem += N * sizeof(magmaFloatComplex);  // S

    void *kernel_args[] = {&dA_array, &ldda, &dipiv_array, &dB_array, &dinfo_array, &dS_array};
    switch(N){
        case  1: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 1>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  2: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 2>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  3: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 3>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  4: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 4>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  5: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 5>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  6: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 6>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  7: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 7>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  8: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 8>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case  9: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm< 9>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 10: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<10>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 11: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<11>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 12: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<12>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 13: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<13>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 14: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<14>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 15: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<15>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 16: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<16>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 17: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<17>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 18: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<18>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 19: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<19>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 20: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<20>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 21: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<21>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 22: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<22>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 23: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<23>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 24: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<24>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 25: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<25>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 26: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<26>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 27: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<27>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 28: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<28>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 29: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<29>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 30: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<30>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 31: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<31>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        case 32: e = cudaLaunchKernel((void*)rand_runge_kutta_fuse_sm<32>, grid, threads, kernel_args, shmem, my_queue->cuda_stream()); break;
        default: e = cudaErrorInvalidValue;
    }
    if( e != cudaSuccess ) {
        printf("cudaLaunchKernel of rand_runge_kutta_fuse_sm is not successful!\n");
    }
  }

}

#endif
