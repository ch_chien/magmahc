#ifndef kernel_get_new_data_compute_k_rk_cu
#define kernel_get_new_data_compute_k_rk_cu
// ============================================================================
// get new data and compute k1, k2, k3, and k4 in runge-kutta method
// for multiple kernel methods
//
// Modifications
//    Chien  21-05-07:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// cuda included
#include <cuda.h>
#include <cuda_runtime.h>

// magma
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"

// -- device function --
#include "magmaHC-dev.cuh"
#include "magmaHC-kernels.h"

namespace magmaHCWrapper {

  // -- get new matrix A and vector b and compute k1 or k4 --
  template<int N>
  __global__ void
  get_new_data_and_compute_k1_k4(
      magmaFloatComplex** dA_array, magma_int_t ldda,
      magmaFloatComplex **dB_array, magmaFloatComplex **dS_array)
  {
    extern __shared__ magmaFloatComplex zdata[];
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x;

    magmaFloatComplex* dA = dA_array[batchid];
    magmaFloatComplex* dB = dB_array[batchid];
    magmaFloatComplex* dS = dS_array[batchid];

    magmaFloatComplex rA[N]  = {MAGMA_C_ZERO};

    magmaFloatComplex  rB = MAGMA_C_ZERO;
    magmaFloatComplex  rS = MAGMA_C_ZERO;

    // -- read --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] = dA[ i * ldda + tx ] + 1;
    }
    rB = dB[tx] + 1;
    rS = dS[tx] + dB[tx] * 1.0/6.0;
    //rS = dS[tx];

    // -- move rA back to dA and rB back to dB --
    #pragma unroll
    for(int i = 0; i < N; i++){
        dA[ i * ldda + tx ] = rA[i];
    }
    dB[ tx ] = rB;
    dS[ tx ] = rS;
  }

  // -- get new matrix A and vector b and compute k2 or k3 --
  template<int N>
  __global__ void
  get_new_data_and_compute_k2_k3(
      magmaFloatComplex** dA_array, magma_int_t ldda,
      magmaFloatComplex **dB_array, magmaFloatComplex **dS_array)
  {
    extern __shared__ magmaFloatComplex zdata[];
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x;

    magmaFloatComplex* dA = dA_array[batchid];
    magmaFloatComplex* dB = dB_array[batchid];
    magmaFloatComplex* dS = dS_array[batchid];

    magmaFloatComplex rA[N]  = {MAGMA_C_ZERO};

    magmaFloatComplex  rB = MAGMA_C_ZERO;
    magmaFloatComplex  rS = MAGMA_C_ZERO;

    // -- read --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] = dA[ i * ldda + tx ] + 1;
    }
    rB = dB[tx] + 1;
    rS = dS[tx] + dB[tx] * 1.0/3.0;

    // -- move rA back to dA and rB back to dB --
    #pragma unroll
    for(int i = 0; i < N; i++){
        dA[ i * ldda + tx ] = rA[i];
    }
    dB[ tx ] = rB;
    dS[ tx ] = rS;
  }

  extern "C" void
  kernel_get_new_data_compute_k_rk(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda,
    magmaFloatComplex **dB_array, magmaFloatComplex **dS_array,
    std::string which_k)
  {
    const magma_int_t thread_x = N;
    dim3 threads(thread_x, 1, 1);
    dim3 grid(batchCount, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    magma_int_t shmem  = 0;
    shmem += N * sizeof(magmaFloatComplex); // B
    shmem += N * sizeof(magmaFloatComplex); // sx
    shmem += N * sizeof(double);             // dsx
    shmem += N * sizeof(int);                // pivot

    void *gdata_kernel_args[] = {&dA_array, &ldda, &dB_array, &dS_array};
    if (which_k == "k1") {
      switch(N){
          case  1: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 1>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  2: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 2>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  3: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 3>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  4: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 4>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  5: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 5>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  6: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 6>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  7: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 7>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  8: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 8>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  9: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 9>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 10: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<10>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 11: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<11>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 12: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<12>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 13: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<13>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 14: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<14>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 15: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<15>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 16: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<16>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 17: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<17>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 18: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<18>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 19: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<19>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 20: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<20>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 21: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<21>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 22: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<22>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 23: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<23>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 24: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<24>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 25: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<25>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 26: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<26>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 27: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<27>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 28: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<28>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 29: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<29>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 30: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<30>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 31: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<31>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 32: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<32>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          default: e = cudaErrorInvalidValue;
      }
      if( e != cudaSuccess ) {
          printf("get new data and compute k1 failed!\n");
      }
    }
    else if (which_k == "k2") {
      switch(N){
          case  1: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 1>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  2: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 2>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  3: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 3>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  4: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 4>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  5: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 5>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  6: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 6>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  7: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 7>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  8: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 8>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  9: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 9>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 10: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<10>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 11: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<11>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 12: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<12>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 13: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<13>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 14: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<14>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 15: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<15>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 16: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<16>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 17: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<17>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 18: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<18>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 19: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<19>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 20: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<20>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 21: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<21>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 22: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<22>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 23: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<23>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 24: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<24>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 25: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<25>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 26: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<26>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 27: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<27>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 28: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<28>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 29: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<29>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 30: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<30>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 31: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<31>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 32: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<32>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          default: e = cudaErrorInvalidValue;
      }
      if( e != cudaSuccess ) {
          printf("get new data and compute k1 failed!\n");
      }
    }
    else if (which_k == "k3") {
      switch(N){
          case  1: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 1>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  2: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 2>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  3: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 3>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  4: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 4>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  5: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 5>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  6: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 6>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  7: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 7>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  8: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 8>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  9: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3< 9>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 10: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<10>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 11: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<11>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 12: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<12>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 13: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<13>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 14: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<14>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 15: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<15>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 16: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<16>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 17: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<17>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 18: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<18>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 19: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<19>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 20: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<20>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 21: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<21>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 22: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<22>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 23: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<23>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 24: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<24>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 25: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<25>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 26: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<26>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 27: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<27>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 28: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<28>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 29: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<29>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 30: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<30>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 31: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<31>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 32: e = cudaLaunchKernel((void*)get_new_data_and_compute_k2_k3<32>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          default: e = cudaErrorInvalidValue;
      }
      if( e != cudaSuccess ) {
          printf("get new data and compute k1 failed!\n");
      }
    }
    else if (which_k == "k4") {
      switch(N){
          case  1: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 1>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  2: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 2>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  3: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 3>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  4: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 4>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  5: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 5>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  6: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 6>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  7: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 7>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  8: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 8>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case  9: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4< 9>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 10: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<10>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 11: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<11>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 12: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<12>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 13: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<13>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 14: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<14>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 15: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<15>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 16: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<16>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 17: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<17>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 18: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<18>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 19: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<19>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 20: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<20>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 21: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<21>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 22: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<22>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 23: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<23>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 24: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<24>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 25: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<25>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 26: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<26>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 27: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<27>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 28: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<28>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 29: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<29>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 30: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<30>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 31: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<31>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          case 32: e = cudaLaunchKernel((void*)get_new_data_and_compute_k1_k4<32>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
          default: e = cudaErrorInvalidValue;
      }
      if( e != cudaSuccess ) {
          printf("get new data and compute k1 failed!\n");
      }
    }
    else {
      std::cout<<"Invalid input of the which_k! Should be either k1, k2, k3, or k4"<<std::endl;
    }
  }
}

#endif
