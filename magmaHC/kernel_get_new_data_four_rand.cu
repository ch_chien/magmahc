#ifndef kernel_get_new_data_four_rand_cu
#define kernel_get_new_data_four_rand_cu
// ============================================================================
// get new data cuda kernel for multiple kernel methods
//
// Modifications
//    Chien  21-05-05:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// cuda included
#include <cuda.h>
#include <cuda_runtime.h>

// magma
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"

// -- device function --
#include "magmaHC-dev.cuh"
#include "magmaHC-kernels.h"

namespace magmaHCWrapper {

  // -- get new matrix A and vector b --
  template<int N>
  __global__ void
  get_new_data(
      magmaFloatComplex** dA_array, magma_int_t ldda,
      magmaFloatComplex **dB_array)
  {
    extern __shared__ magmaFloatComplex zdata[];
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x;

    magmaFloatComplex* dA = dA_array[batchid];
    magmaFloatComplex* dB = dB_array[batchid];

    magmaFloatComplex rA[N]  = {MAGMA_C_ZERO};
    int linfo = 0, rowid = tx;

    magmaFloatComplex  rB = MAGMA_C_ZERO;

    // -- read --
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] = dA[ i * ldda + tx ] + 1;
    }
    rB = dB[tx] + 1;

    // -- move rA back to dA and rB back to dB --
    #pragma unroll
    for(int i = 0; i < N; i++){
        dA[ i * ldda + rowid ] = rA[i];
    }
    dB[ tx ] = rB;
  }

  extern "C" void
  kernel_get_new_data_four_rand(
    magma_int_t N, magma_int_t batchCount, magma_queue_t my_queue,
    magmaFloatComplex** dA_array, magma_int_t ldda,
    magmaFloatComplex **dB_array)
  {
    const magma_int_t thread_x = N;
    dim3 threads(thread_x, 1, 1);
    dim3 grid(batchCount, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    magma_int_t shmem  = 0;
    shmem += N * sizeof(magmaFloatComplex); // B
    shmem += N * sizeof(magmaFloatComplex); // sx
    shmem += N * sizeof(double);             // dsx
    shmem += N * sizeof(int);                // pivot

    void *gdata_kernel_args[] = {&dA_array, &ldda, &dB_array};
    switch(N){
        case  1: e = cudaLaunchKernel((void*)get_new_data< 1>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  2: e = cudaLaunchKernel((void*)get_new_data< 2>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  3: e = cudaLaunchKernel((void*)get_new_data< 3>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  4: e = cudaLaunchKernel((void*)get_new_data< 4>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  5: e = cudaLaunchKernel((void*)get_new_data< 5>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  6: e = cudaLaunchKernel((void*)get_new_data< 6>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  7: e = cudaLaunchKernel((void*)get_new_data< 7>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  8: e = cudaLaunchKernel((void*)get_new_data< 8>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case  9: e = cudaLaunchKernel((void*)get_new_data< 9>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 10: e = cudaLaunchKernel((void*)get_new_data<10>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 11: e = cudaLaunchKernel((void*)get_new_data<11>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 12: e = cudaLaunchKernel((void*)get_new_data<12>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 13: e = cudaLaunchKernel((void*)get_new_data<13>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 14: e = cudaLaunchKernel((void*)get_new_data<14>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 15: e = cudaLaunchKernel((void*)get_new_data<15>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 16: e = cudaLaunchKernel((void*)get_new_data<16>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 17: e = cudaLaunchKernel((void*)get_new_data<17>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 18: e = cudaLaunchKernel((void*)get_new_data<18>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 19: e = cudaLaunchKernel((void*)get_new_data<19>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 20: e = cudaLaunchKernel((void*)get_new_data<20>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 21: e = cudaLaunchKernel((void*)get_new_data<21>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 22: e = cudaLaunchKernel((void*)get_new_data<22>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 23: e = cudaLaunchKernel((void*)get_new_data<23>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 24: e = cudaLaunchKernel((void*)get_new_data<24>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 25: e = cudaLaunchKernel((void*)get_new_data<25>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 26: e = cudaLaunchKernel((void*)get_new_data<26>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 27: e = cudaLaunchKernel((void*)get_new_data<27>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 28: e = cudaLaunchKernel((void*)get_new_data<28>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 29: e = cudaLaunchKernel((void*)get_new_data<29>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 30: e = cudaLaunchKernel((void*)get_new_data<30>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 31: e = cudaLaunchKernel((void*)get_new_data<31>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        case 32: e = cudaLaunchKernel((void*)get_new_data<32>, grid, threads, gdata_kernel_args, shmem, my_queue->cuda_stream()); break;
        default: e = cudaErrorInvalidValue;
    }
    if( e != cudaSuccess ) {
        printf("get new data failed!\n");
    }
  }
}

#endif
