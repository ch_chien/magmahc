#ifndef magmaHC_methods_cuh
#define magmaHC_methods_cuh
// =======================================================================
// Header file for declaring fused kernel and multiple kernel methods
//
// Modifications
//    Chien  21-05-05:   Add four_rand_fuse, four_rand_mulk,
//                       and check_correctness
//
// =======================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include "magma_v2.h"

namespace magmaHCWrapper {

    void four_batched_fused_kernel(int batchSize, int matrixSize, std::string fuse_method);
    void four_batched_multiple_kernels(int batchSize, int matrixSize);
    void runge_kutta_fused_kernel(int batchSize, int matrixSize, std::string fuse_method);
    void runge_kutta_multiple_kernels(int batchSize, int matrixSize);

    void check_correctness(
      magma_int_t batchCount, magma_int_t *dinfo_array,
      magma_int_t *cpu_info, magma_queue_t my_queue
    );
}

#endif
