## MAGAM CGESV RANDOMIZED FOUR BATCHED LINEAR SYSTEMS SOLVER ##

# 1. How to use the code
(1) clone the repo
```bash
git clone <git clone https://ch_chien@bitbucket.org/ch_chien/magmahc.git>
```
(2) under the repo folder, create a 'build' directory
```bash
mkdir build
```
(3) enter the build folder
```bash
cd build
```
(4) create a make file
```bash
cmake ..
```
(5) compile the entire code
```bash
make -j
```
(6) enter the bin foler
```bash
cd bin
```
(7) run the code by specifically typing input arguments
```bash
./magmaHC-main <input-argument> <command>
```

## Input arguments and the associated commands are listed below:
```
<input-argument> <command>
      -t         <method>   # (or --test_method)  : test algorithm, <method> mist be either fb (four batched linear systems) or rk (runge-kutta)
      -b         <value>    # (or --batchSize)    : batch size of linear systems Ax=b, specified by <value>
      -n         <value>    # (or --matrixSize)   : matrix size of A, specified by <value>
      -f         <method>   # (or --fuse)         : using a fused kernel to solve batched Ax=b, <method> can be either gm (global memory) or reg (registers)
      -m                    # (or --mulipltKernel): using multiple kernels to solve batched Ax=b
      -h                    # (or --help)         : print this help message    
```
      
## Notice:
```
1. Choose either -f or -m, but not both.
2. Order matters. Either -f or -m has to be the last argument.
3. If not specified, the help message will be automatically shown.
```

## Execution examples:
```
./magmaHC-main -t fb -b 1024 -n 6 -f reg   # solve randomized four batched Ax=b problem using registers in the fused kernel, where matrix A is 6x6, and the batch size is 1024.
./magmaHC-main -t rk -b 2048 -n 18 -m      # solve randomized Runge-Kutta problem using multiple kernels, where matrix A is 18x18, and the batch size is 2048.
```
  
# 2. Additional notices:
```
(1) You need to change the directories where the updated magma "gesv_batched branch" is installed in
    line 113~116 in magmahc/CMakeLists.txt
    line 25~28   in magmahc/magmaHC/CMakeLists.txt
(2) You may also need to change the installed libraries of cuda and magma in
    line 30	     in magmahc/magmaHC/CMakeLists.txt
(3) The flags passed to the nvcc compiler can be found in
    line 38      in magmahc/magmaHC/CMakeLists.txt
```
