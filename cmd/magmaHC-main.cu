#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <chrono>
// ============================================================================
// main function
//
// Modifications
//    Chien  21-05-03:   Originally created
//
// ============================================================================
// -- input cmd --
#include "magmaHC/input-info.h"

// -- magma --
#include "magmaHC/magmaHC-methods.cuh"

int main(int argc, char **argv) {
  --argc; ++argv;
  std::string arg;
  int N;
  int batchSize;
  int argIndx = 0;
  int argTotal = 8;
  bool useCase = 0;
  std::string fuse_method = "not using fuse method";
  std::string test_method = "default";

  if (argc) {
    arg = std::string(*argv);
    if (arg == "-h" || arg == "--help") {
      cmdInputs::print_usage();
      exit(1);
    }
    else if (argc <= argTotal) {
      while(argIndx <= argTotal-1) {
        if (arg == "-t" || arg == "--test") {
          argv++;
          arg = std::string(*argv);
          test_method = arg;
          argIndx+=2;
        }
        else if (arg == "-b" || arg == "--batchSize") {
          argv++;
          arg = std::string(*argv);
          batchSize = std::stoi(arg);
          argIndx+=2;
        }
        else if (arg == "-n" || arg == "--matrixSize") {
          argv++;
          arg = std::string(*argv);
          N = std::stoi(arg);
          argIndx+=2;
        }
        else if (arg == "-f" || arg == "--fuse") {
          useCase = 0;
          argv++;
          arg = std::string(*argv);
          fuse_method = arg;
          argIndx+=2;
          break;
        }
        else if (arg == "-m" || arg == "--mulipltKernel") {
          useCase = 1;
          argIndx++;
          break;
        }
        else {
          std::cerr<<"invalid input arguments! See examples: \n";
          cmdInputs::print_usage();
          exit(1);
        }
        argv++;
        arg = std::string(*argv);
      }
    }
    else if (argc > argTotal) {
      std::cerr<<"too many arguments!\n";
      cmdInputs::print_usage();
      exit(1);
    }
  }
  else {
    cmdInputs::print_usage();
    exit(1);
  }

  if (test_method == "fb") {
    if (!useCase)
      magmaHCWrapper::four_batched_fused_kernel(batchSize, N, fuse_method);
    else
      magmaHCWrapper::four_batched_multiple_kernels(batchSize, N);
  }
  else if (test_method == "rk") {
    if (!useCase)
      magmaHCWrapper::runge_kutta_fused_kernel(batchSize, N, fuse_method);
    else
      magmaHCWrapper::runge_kutta_multiple_kernels(batchSize, N);
  }
  return 0;
}
